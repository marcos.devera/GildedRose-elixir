defmodule GildedRoseTest do
  use ExUnit.Case, async: true
  doctest GildedRose

  test "Once the sell by date has passed, Quality degrades twice as fast" do
    before_list = [
      %Item{name: "lettuce", sell_in: -1, quality: 50},
      %Item{name: "lemon", sell_in: 0, quality: 50}
    ]

    after_list = [
      %Item{name: "lettuce", sell_in: -2, quality: 48},
      %Item{name: "lemon", sell_in: -1, quality: 49}
    ]

    assert GildedRose.update_items(before_list) == after_list
  end

  test "The Quality of an item is never negative" do
    before_list = [
      %Item{name: "lettuce", sell_in: 0, quality: 0},
      %Item{name: "Conjured", sell_in: 0, quality: 0}
    ]

    after_list = [
      %Item{name: "lettuce", sell_in: -1, quality: 0},
      %Item{name: "Conjured", sell_in: -1, quality: 0}
    ]

    assert GildedRose.update_items(before_list) == after_list
  end

  test "\"Aged Brie\" actually increases in Quality the older it gets" do
    before_list = [
      %Item{name: "Aged Brie", sell_in: 0, quality: 0},
      %Item{name: "Aged Brie", sell_in: 0, quality: 50}
    ]

    after_list = [
      %Item{name: "Aged Brie", sell_in: -1, quality: 1},
      %Item{name: "Aged Brie", sell_in: -1, quality: 50}
    ]

    assert GildedRose.update_items(before_list) == after_list
  end

  test "The Quality of an item is never more than 50" do
    before_list = [
      %Item{name: "Aged Brie", sell_in: 0, quality: 50},
      %Item{name: "Backstage passes", sell_in: 2, quality: 50}
    ]

    after_list = [
      %Item{name: "Aged Brie", sell_in: -1, quality: 50},
      %Item{name: "Backstage passes", sell_in: 1, quality: 50}
    ]

    assert GildedRose.update_items(before_list) == after_list
  end

  test "\"Sulfuras\", being a legendary item, never has to be sold or decreases in Quality" do
    before_list = [%Item{name: "Sulfuras", sell_in: 2, quality: 30}]
    after_list = [%Item{name: "Sulfuras", sell_in: 2, quality: 80}]
    assert GildedRose.update_items(before_list) == after_list
  end

  test "\"Backstage passes\" increase in quality as sell_in approaches" do
    before_list = [
      %Item{name: "Backstage passes", sell_in: 10, quality: 40},
      %Item{name: "Backstage passes", sell_in: 5, quality: 40},
      %Item{name: "Backstage passes", sell_in: -1, quality: 40},
      %Item{name: "Backstage passes", sell_in: 20, quality: 40}
    ]

    after_list = [
      %Item{name: "Backstage passes", sell_in: 9, quality: 42},
      %Item{name: "Backstage passes", sell_in: 4, quality: 43},
      %Item{name: "Backstage passes", sell_in: -2, quality: 0},
      %Item{name: "Backstage passes", sell_in: 19, quality: 39}
    ]

    assert GildedRose.update_items(before_list) == after_list
  end

  test "Conjured items degrade as twice as fast" do
    before_list = [%Item{name: "Conjured", sell_in: 2, quality: 30}]
    after_list = [%Item{name: "Conjured", sell_in: 1, quality: 28}]
    assert GildedRose.update_items(before_list) == after_list
  end
end
